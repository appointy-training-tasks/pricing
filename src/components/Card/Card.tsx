import React from "react";
import "./Card.css";
import { Pricing } from "../../shared/pricing";

interface Props {
  pricing: Pricing;
  selected: boolean;
  handleOnClickPricingCard: React.MouseEventHandler;
}

const Card = ({ pricing, selected, handleOnClickPricingCard }: Props) => {
  return (
    <div className="card">
      <div className="card__header">
        <h4 className="card__headerHeading">{pricing.type}</h4>
        {pricing.isMostPopular && (
          <>
            <p className="card__headerSubHeading">Most Popular</p>
            <span className="card__headerIcon">☆</span>
          </>
        )}
      </div>
      <div className="card__body">
        <div className="card__bodyLead">
          <p className="card__bodyContent">
            ${pricing.costPerMonth}
            <span>/mo</span>
          </p>
        </div>
        <p className="card__bodyContent">
          {pricing.usersIncluded} users included
        </p>
        <p className="card__bodyContent">{pricing.storage} GB of storage</p>
        {pricing.features?.map((feature) => (
          <p className="card__bodyContent">{feature}</p>
        ))}
      </div>
      <div className="card__footer">
        <button
          className={`card__footerLink ${
            selected && "card__footerLink--selected"
          }`}
          onClick={handleOnClickPricingCard}>
          {pricing.link}
        </button>
      </div>
    </div>
  );
};

export default Card;
