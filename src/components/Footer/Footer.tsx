import React from "react";
import footerCategories from "../../shared/footer";
import "./Footer.css";

import { v4 as uuid } from "uuid";

const Footer = () => {
  return (
    <footer className="footer">
      <ul className="footer__categories">
        {footerCategories.map(([footerCategoryLead, footerCategoryLinks]) => (
          <li className="footer__category" key={uuid()}>
            <h6 className="footer__categoryLead">{footerCategoryLead}</h6>
            <ul className="footer__categoryLinks">
              {footerCategoryLinks.map((link) => (
                <li className="footer__categoryLink" key={uuid()}>
                  {link}
                </li>
              ))}
            </ul>
          </li>
        ))}
      </ul>
    </footer>
  );
};

export default Footer;
