import React from "react";
import "./Header.css";

const Header = () => {
  return (
    <header className="header">
      <h1 className="header__heading">Pricing</h1>
      <p className="header__content">
        Quickly build an effective pricing table for your potential customers
        with this layout. It's built with default Material-UI components with
        little customization.
      </p>
    </header>
  );
};

export default Header;
