import React, { useState } from "react";
import pricingDetails from "../../shared/pricing";
import Card from "../Card/Card";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";
import "./App.css";

import { v4 as uuid } from "uuid";

function App() {
  const [selectedCardId, setSelectedCardId] = useState(pricingDetails[1].id);

  const updatedSelectedCardId = (id: string) => {
    setSelectedCardId(id);
  };

  return (
    <div className="app">
      <Header />
      <main>
        <ul className="pricing__cards">
          {pricingDetails.map((pricing) => (
            <li className="pricing__card" key={uuid()}>
              <Card
                pricing={pricing}
                selected={selectedCardId === pricing.id}
                handleOnClickPricingCard={() =>
                  updatedSelectedCardId(pricing.id)
                }
              />
            </li>
          ))}
        </ul>
      </main>
      <hr />
      <Footer />
    </div>
  );
}

export default App;
