import { v4 as uuid } from "uuid";

export interface Pricing {
  id: string;
  type: string;
  isMostPopular: boolean;
  costPerMonth: number;
  usersIncluded: number;
  storage: number;
  features: string[];
  link: string;
}

const pricingDetails: Pricing[] = [
  {
    id: uuid(),
    type: "Free",
    isMostPopular: false,
    costPerMonth: 0,
    usersIncluded: 10,
    storage: 2,
    features: ["Help center access", "Email support"],
    link: "Sign up for free",
  },
  {
    id: uuid(),
    type: "Pro",
    isMostPopular: true,
    costPerMonth: 15,
    usersIncluded: 20,
    storage: 10,
    features: ["Help center access", "Priority email support"],
    link: "Get started",
  },
  {
    id: uuid(),
    type: "Enterprise",
    isMostPopular: false,
    costPerMonth: 30,
    usersIncluded: 50,
    storage: 30,
    features: ["Help center access", "Phone & email support"],
    link: "Contact us",
  },
];

export default pricingDetails;
