type FooterCategory = [string, string[]];

const footerCategories: FooterCategory[] = [
  ["Company", ["Team", "History", "Contact us", "Locations"]],
  [
    "Features",
    [
      "Cool stuff",
      "Random feature",
      "Team feature",
      "Developer stuff",
      "Another one",
    ],
  ],
  [
    "Resources",
    ["Resource", "Resource name", "Another resource", "Final resource"],
  ],
  ["Legal", ["Privacy Policy", "Terms of use"]],
];

export default footerCategories;
